########################################################################

# check for lates xyls file in fits_files directory ... came from Mookodi PC
# if there then send to astronometry
# then scp wcs solution to mookodi PC

# this program run as a systemctl process:
#/etc/systemd/system/xyls_astrometry_WCS.service   ... included in git here


########################################################################

import os
import argparse
import sys
from astropy.io import fits
from astropy.coordinates import SkyCoord
from astropy import units as u
import datetime
import time
import numpy as np
import matplotlib.pyplot as plt
import astropy.visualization
zscaler = astropy.visualization.ZScaleInterval()
from photutils import CircularAperture
import sep
from collections import OrderedDict
import math 

from astroquery.gaia import Gaia



########################################################################
#########################      MAIN       ##############################
########################################################################		
def main(file_done_already):
        

    ### get lastest xyls file in fits_files directory ####                                                                           
    #### this file was created by Mookodi_source_extract.py on the mookodi PC and scp to here fits_files ###
    
    ls_command = ' ls fits_files/*.xyls'
    output_part = ' > terminal_ouput_file.txt'
    final_command = ls_command+output_part
    #print('Checking latest file')
    #print(final_command)                                                                                                  
    os.system(final_command)
    with open('terminal_ouput_file.txt') as f:
        for line in f:
            pass
        latest_file= line.strip()
    #print('Latest and done files are: '+ latest_file + ' ' + file_done_already)


    if (latest_file == file_done_already): # if the lates file has already been procssed then return
        return(file_done_already)


    ### open xyls and get some header info to feed astrometry.net eg. RA and Dec as first guess pointing ###                                          
    data_hdu = fits.open(latest_file)
    mookodi_header = data_hdu[0].header
    #print(mookodi_header)
    #data_array = data_hdu[0].data
    mookodi_ra = mookodi_header['TELRA']
    mookodi_dec = mookodi_header['TELDEC']
    #telescope_coords = SkyCoord(mookodi_ra, mookodi_dec,unit=(u.hourangle, u.deg))
    telescope_coords = SkyCoord(mookodi_ra, mookodi_dec,unit=(u.deg, u.deg))
    ra_in_deg = telescope_coords.to_string().split()[0]
    dec_in_deg = telescope_coords.to_string().split()[1]
    binning = mookodi_header['HBIN']

    print(ra_in_deg, dec_in_deg )

    data_hdu.close()
    
    ##############################################################################################
    # run the .xyls file through astrometry.net
    ##############################################################################################
    
    search_radius =  '0.5' # telling astropy to search within 0.5 degrees of header RA and Dec values.
    ## search between these two values for the solved FoV                                                                                               
    scale_low  = '0.1' # mookodi in 1x1 is 10x10arcmins = 0.16X0.16degs 
    scale_high  = '0.2' 

    image_size_x = 1024/binning
    image_size_y = 1024/binning
    
    try:#### astrometry.net command on server and write results to wcs_info.txt .... send .xyls file to local server ie not the fits image:
        astr_command = '/usr/local/astrometry/bin/solve-field --overwrite --scale-units degwidth --scale-low '+scale_low+' --scale-high '+scale_high+' --ra '+ra_in_deg+' --dec '+dec_in_deg+' --radius '+search_radius+'  --x-column x --y-column y --sort-column flux --width '+str(image_size_x)+' --height '+str(image_size_y)+' --no-plots  --new-fits none '+latest_file
        output_part = ' > '+latest_file.replace('.xyls','.WCS')
        final_command = astr_command+output_part
        print('Running astrometry.net command on .xyls...')
        os.system(final_command)
        #print('Writing solution to MKDxxx.WCS and scp to Mookodi')
        # scp solution file to Mookodi
        scp_command = "scp " + latest_file.replace('.xyls','.WCS') + " mookodi@mookodi.suth.saao.ac.za:/home/mookodi/development/WCS/fits_files/"
        #print(scp_command)
        os.system(scp_command)
        return(latest_file)
    except Exception as e:
        #print('Could not run astrometry.net on server. Exiting.')
        print(sys.stderr, "Exception: %s" % str(e))
        return(latest_file)#        sys.exit(1)

    
if __name__ == "__main__":
    file_done = main("1st")
    while 1:
        file_done = main(file_done)
        time.sleep(3)
